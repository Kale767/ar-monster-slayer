# AR Game
An AR Image Tracking Game, that relies on designed cards to spawn numerous objects. This game make use of poker cards that I drew onto so that each image can be recognized. A Battle Platform (created from multiple cards) must be scanned first to spawn the battle area, from this point the player must then scan their character card to spawn their character into the game. From this point the player must place cards on their designated spots until an enemy card is scanned and the enemy is spawned.

Using Unity AR Foundation

# Cards
Card images can be found in the Runic Images folder. 

# System Requirements (Mobile)

Android Supported Device:

That is capable of using AR Core and AR Foundation.

Min API Level: Android 7.0 'Nougat' (API Level 24)


# User Instructions
Start by Installing the APK file to a mobile device that is AR capable.

Place the Battle Ground in front of yourself, with room around yourself.

Shuffle the Card Deck, all cards except the for the two Ace of Spades cards.

Start the Game

Place your player card into the designated slot on the left of the board. (Male or Female)

Once the Deck is shuffled, place the first card from the top of the deck onto its designated area, you can check the back of each card to see which area it falls under, because these images correlate to the images on the Battle Board.

When you see your first enemy NPC by scanning its relevant card, you must stop placing cards until the fight is over.

If any prefabs move out of place, re - scan the card again, including the Battle Ground.

# Link to Youtube tutorial series
https://www.youtube.com/watch?v=_1pz_ohupPs&ab_channel=Brackeys

